'use strict';

angular.module('app').controller('MainController', function($scope) {

    $scope.data = {};
    $scope.data.mainCheck = "I am accessed from the Main Controller.";
});

angular.module('app').controller('DashboardController', function($scope) {

    $scope.data = {};
    $scope.data.header = "Dashboard View";
    $scope.data.quote = "AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop.";
    $scope.data.source = "From Angular's website";
});

angular.module('app').controller('ProjectController', function($scope) {

    $scope.data = {};
    $scope.data.header = "Project View";
    $scope.data.header2 = "Data Binding Demonstration";
    $scope.data.project = {
        name: "Operation Pass Emerging Trends",
        type: "Red",
        status: "Ongoing"
    }
});