'use strict';

angular.module('app').config(function ($routeProvider){

    $routeProvider.
    when('/Dashboard', {
        templateUrl: "dashboard.html",
        controller: "DashboardController"
    }).
    when('/Projects', {
        templateUrl: "projects.html",
        controller: "ProjectController"
    }).
    otherwise({
        redirectTo: '/Dashboard'
    });
});
